import java.io.File;  
import org.apache.pdfbox.pdmodel.PDDocument; 
import org.apache.pdfbox.pdmodel.PDPage; 
import org.apache.pdfbox.pdmodel.PDPageContentStream; 
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;  
public class InsertingImageInPdf
 { 
   public static void main(String args[]) throws Exception 
   {  
      File file = new File("C:/pdfBox/InsertImage_IP.pdf"); 
      PDDocument doc = PDDocument.load(file);  
      PDPage page = doc.getPage(0);  
      PDImageXObject pdImage = PDImageXObject.createFromFile("C:/pdfBox/logo.png", doc); 
      PDPageContentStream contents = new PDPageContentStream(doc, page); 
      contents.drawImage(pdImage, 70, 250);     
      System.out.println("Image inserted"); 
      contents.close();        
      doc.save("C:/pdfBox/InsertImage_OP.pdf"); 
      doc.close(); 
   }  
} 
